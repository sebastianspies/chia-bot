import os
import shutil
import sys

from flask import request, Flask
import subprocess
import pathlib
app = Flask(__name__)

CHANNEL = os.getenv('CHANNEL')
USER = os.getenv('USER')

if not CHANNEL or not USER:
    print("specify env variables CHANNEL and USER ")
    sys.exit(-1)

CLI = "/app/signal-cli-0.8.1/bin/signal-cli"


@app.route("/", methods=['POST'])
def send_message():
    """
    {
      "dashboardId":1,
      "evalMatches":[
        {
          "value":1,
          "metric":"Count",
          "tags":{}
        }
      ],
      "imageUrl":"https://grafana.com/assets/img/blog/mixed_styles.png",
      "message":"Notification Message",
      "orgId":1,
      "panelId":2,
      "ruleId":1,
      "ruleName":"Panel Title alert",
      "ruleUrl":"http://localhost:3000/d/hZ7BuVbWz/test-dashboard?fullscreen\u0026edit\u0026tab=alert\u0026panelId=2\u0026orgId=1",
      "state":"alerting",
      "tags":{
        "tag name":"tag value"
      },
      "title":"[Alerting] Panel Title alert"
    }
    :return:
    """

    data = request.json

    message = data['message']
    if "state" in data:
        message += f", state: {data['state']}"

    if "evalMatches" in data:
        for match in data["evalMatches"]:
            message += f", {match['metric']} = {match['value']}"

    command = f"{CLI} receive -t 5"
    subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()

    command = f"{CLI} -u {USER} send -g \"{CHANNEL}\" -m \"{message}\""
    output = subprocess.Popen(command, shell=True,
                              stdout=subprocess.PIPE).stdout.read()

    return output

