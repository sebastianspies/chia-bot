FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update; apt-get install -y --no-install-recommends openjdk-11-jre python3.8 python3-pip tzdata


WORKDIR /app
ADD https://github.com/AsamK/signal-cli/releases/download/v0.8.1/signal-cli-0.8.1.tar.gz /app
RUN ls -lah /app/; tar -xvf signal-cli-0.8.1.tar.gz
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt
COPY app.py .

EXPOSE 8080
CMD ["flask", "run", "-h", "0.0.0.0", "-p", "8080"]



